from bs4 import BeautifulSoup as bf
from urllib.request import urlopen
import json
import datetime


def fetch(url, generator):
    infos = []

    html_content = urlopen(url)
    content = bf(html_content.read(), "html.parser")
    # print(content)
    content4today = content.body.find_all("dl")[0]
    dds = content4today.find_all("dd")
    dts = content4today.find_all("dt")

    assert len(dds) == len(dts)
    for dd, dt in zip(dds, dts):  # Every Block
        title = dd.find_all("div", attrs={"class": "list-title"})[0].text
        title = title.replace("\n", " ").replace("Title: ","")
        authors = [a.text for a in dd.find_all("a")]
        pdf_path = (
            "https://arxiv.org" + dt.find_all("span")[0].find_all("a")[1]["href"]
        )

        ## abs
        abstract_path = (
            "https://arxiv.org" + dt.find_all("span")[0].find_all("a")[0]["href"]
        )
        infos.append(
            {
                "t": title,
                "a": authors,
                "abs": abstract_path,
                "pdf": pdf_path,
                "abs_text": "获取中",
            }
        )
    date = datetime.date.strftime(datetime.date.today(), "%Y%m%d")
    generator(date, infos)


def toJson(date, infos):
    with open("{}-arxiv.json".format(date), "w+") as f:
        json.dump({"infos":infos}, f)

def toMarkdown(date, infos):
    with open("{}-arxiv.md".format(date), "w+") as f:
        f.write("# {} Papers\n\n".format(date))
        for info in infos:
            content = "### {}\n\nauthors: {}\n\nabstract: \t{}\n\npdf_url:\t{}\n\n".format(
                info["t"], 
                info["a"],
                info["abs"],
                info["pdf"])
            f.write(content)
            f.write("---\n\n")

def keyword_filter(infos, keyword=None):
    filtered_content = []
    if keyword is None:
        return infos
    if infos is None:
        print("No info to be filter")
        return
    for info in infos:
        keyword = keyword.lower()
        if info["t"].lower().find(keyword) >= 0:
            filtered_content.append(info)
        else:
            for author in info["a"]:
                if author.lower().find(keyword) >= 0:
                    filtered_content.append(info)
                    break
    return filtered_content

if __name__ == "__main__":
    url = "https://arxiv.org/list/cs/pastweek?skip=0&show=500"
    fetch(url, toMarkdown)