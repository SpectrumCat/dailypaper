# Generating Visual Scenes from Touch 

authors: ['Fengyu Yang', 'Jiacheng Zhang', 'Andrew Owens', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.15117

pdf_url:	https://arxiv.org/pdf/2309.15117

# InternLM-XComposer: A Vision-Language Large Model for Advanced  Text-image Comprehension and Composition 

authors: ['Pan Zhang', 'Xiaoyi Dong Bin Wang', 'Yuhang Cao', 'Chao Xu', 'Linke Ouyang', 'Zhiyuan Zhao', 'Shuangrui Ding', 'Songyang Zhang', 'Haodong Duan', 'Hang Yan', 'Xinyue Zhang', 'Wei Li', 'Jingwen Li', 'Kai Chen', 'Conghui He', 'Xingcheng Zhang', 'Yu Qiao', 'Dahua Lin', 'Jiaqi Wang', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.15112

pdf_url:	https://arxiv.org/pdf/2309.15112

# Doduo: Learning Dense Visual Correspondence from Unsupervised  Semantic-Aware Flow 

authors: ['Zhenyu Jiang', 'Hanwen Jiang', 'Yuke Zhu', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.15110

pdf_url:	https://arxiv.org/pdf/2309.15110

# DistillBEV: Boosting Multi-Camera 3D Object Detection with Cross-Modal  Knowledge Distillation 

authors: ['Zeyu Wang', 'Dingwen Li', 'Chenxu Luo', 'Cihang Xie', 'Xiaodong Yang']

abstract: 	https://arxiv.org/abs/2309.15109

pdf_url:	https://arxiv.org/pdf/2309.15109

# LAVIE: High-Quality Video Generation with Cascaded Latent Diffusion  Models 

authors: ['Yaohui Wang', 'Xinyuan Chen', 'Xin Ma', 'Shangchen Zhou', 'Ziqi Huang', 'Yi Wang', 'Ceyuan Yang', 'Yinan He', 'Jiashuo Yu', 'Peiqing Yang', 'Yuwei Guo', 'Tianxing Wu', 'Chenyang Si', 'Yuming Jiang', 'Cunjian Chen', 'Chen Change Loy', 'Bo Dai', 'Dahua Lin', 'Yu Qiao', 'Ziwei Liu', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.15103

pdf_url:	https://arxiv.org/pdf/2309.15103

# Case Study: Ensemble Decision-Based Annotation of Unconstrained Real  Estate Images 

authors: ['Miroslav Despotovic', 'Zedong Zhang', 'Eric Stumpe', 'Matthias Zeppelzauer']

abstract: 	https://arxiv.org/abs/2309.15097

pdf_url:	https://arxiv.org/pdf/2309.15097

# VideoDirectorGPT: Consistent Multi-scene Video Generation via LLM-Guided  Planning 

authors: ['Han Lin', 'Abhay Zala', 'Jaemin Cho', 'Mohit Bansal', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.15091

pdf_url:	https://arxiv.org/pdf/2309.15091

# Video-adverb retrieval with compositional adverb-action embeddings 

authors: ['Thomas Hummel', 'Otniel-Bogdan Mercea', 'A. Sophia Koepke', 'Zeynep Akata']

abstract: 	https://arxiv.org/abs/2309.15086

pdf_url:	https://arxiv.org/pdf/2309.15086

# he Surveillance AI Pipeline 

authors: ['Pratyusha Ria Kalluri', 'William Agnew', 'Myra Cheng', 'Kentrell Owens', 'Luca Soldaini', 'Abeba Birhane']

abstract: 	https://arxiv.org/abs/2309.15084

pdf_url:	https://arxiv.org/pdf/2309.15084

# RPEFlow: Multimodal Fusion of RGB-PointCloud-Event for Joint Optical  Flow and Scene Flow Estimation 

authors: ['Zhexiong Wan', 'Yuxin Mao', 'Jing Zhang', 'Yuchao Dai', 'this https URL', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.15082

pdf_url:	https://arxiv.org/pdf/2309.15082

# Nuclear Morphometry using a Deep Learning-based Algorithm has Prognostic  Relevance for Canine Cutaneous Mast Cell Tumors 

authors: ['Andreas Haghofer', 'Eda Parlak', 'Alexander Bartel', 'Taryn A. Donovan', 'Charles-Antoine Assenmacher', 'Pompei Bolfa', 'Michael J. Dark', 'Andrea Fuchs-Baumgartinger', 'Andrea Klang', 'Kathrin Jäger', 'Robert Klopfleisch', 'Sophie Merz', 'Barbara Richter', 'F. Yvonne Schulman', 'Jonathan Ganz', 'Josef Scharinger', 'Marc Aubreville', 'Stephan M. Winkler', 'Matti Kiupel', 'Christof A. Bertram']

abstract: 	https://arxiv.org/abs/2309.15031

pdf_url:	https://arxiv.org/pdf/2309.15031

# IFT: Image Fusion Transformer for Ghost-free High Dynamic Range Imaging 

authors: ['Hailing Wang', 'Wei Li', 'Yuanyuan Xi', 'Jie Hu', 'Hanting Chen', 'Longyu Li', 'Yunhe Wang']

abstract: 	https://arxiv.org/abs/2309.15019

pdf_url:	https://arxiv.org/pdf/2309.15019

# Unidirectional brain-computer interface: Artificial neural network  encoding natural images to fMRI response in the visual cortex 

authors: ['Ruixing Liang', 'Xiangyu Zhang', 'Qiong Li', 'Lai Wei', 'Hexin Liu', 'Avisha Kumar', 'Kelley M. Kempski Leadingham', 'Joshua Punnoose', 'Leibny Paola Garcia', 'Amir Manbachi']

abstract: 	https://arxiv.org/abs/2309.15018

pdf_url:	https://arxiv.org/pdf/2309.15018

# Object-Centric Open-Vocabulary Image-Retrieval with Aggregated Features 

authors: ['Hila Levi', 'Guy Heller', 'Dan Levi', 'Ethan Fetaya']

abstract: 	https://arxiv.org/abs/2309.14999

pdf_url:	https://arxiv.org/pdf/2309.14999

# An Ensemble Model for Distorted Images in Real Scenarios 

authors: ['Boyuan Ji', 'Jianchang Huang', 'Wenzhuo Huang', 'Shuke He']

abstract: 	https://arxiv.org/abs/2309.14998

pdf_url:	https://arxiv.org/pdf/2309.14998

# IAIFNet: An Illumination-Aware Infrared and Visible Image Fusion Network 

authors: ['Qiao Yang', 'Yu Zhang', 'Jian Zhang', 'Zijing Zhao', 'Shunli Zhang', 'Jinqiao Wang', 'Junzhe Chen']

abstract: 	https://arxiv.org/abs/2309.14997

pdf_url:	https://arxiv.org/pdf/2309.14997

# Robust Sequential DeepFake Detection 

authors: ['Rui Shao', 'Tianxing Wu', 'Ziwei Liu', 'arXiv:2207.02204', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.14991

pdf_url:	https://arxiv.org/pdf/2309.14991

# MoCaE: Mixture of Calibrated Experts Significantly Improves Object  Detection 

authors: ['Kemal Oksuz', 'Selim Kuzucu', 'Tom Joy', 'Puneet K. Dokania']

abstract: 	https://arxiv.org/abs/2309.14976

pdf_url:	https://arxiv.org/pdf/2309.14976

# Improving Unsupervised Visual Program Inference with Code Rewriting  Families 

authors: ['Aditya Ganeshan', 'R. Kenny Jones', 'Daniel Ritchie', 'this https URL']

abstract: 	https://arxiv.org/abs/2309.14972

pdf_url:	https://arxiv.org/pdf/2309.14972

# A novel approach for holographic 3D content generation without depth map 

authors: ['Hakdong Kim', 'Minkyu Jee', 'Yurim Lee', 'Kyudam Choi', 'MinSung Yoon', 'Cheongwon Kim']

abstract: 	https://arxiv.org/abs/2309.14967

pdf_url:	https://arxiv.org/pdf/2309.14967

# GridFormer: Towards Accurate Table Structure Recognition via Grid  Prediction 

authors: ['Pengyuan Lyu', 'Weihong Ma', 'Hongyi Wang', 'Yuechen Yu', 'Chengquan Zhang', 'Kun Yao', 'Yang Xue', 'Jingdong Wang']

abstract: 	https://arxiv.org/abs/2309.14962

pdf_url:	https://arxiv.org/pdf/2309.14962

# Multi-Source Domain Adaptation for Object Detection with Prototype-based  Mean-teacher 

authors: ['Atif Belal', 'Akhil Meethal', 'Francisco Perdigon Romero', 'Marco Pedersoli', 'Eric Granger']

abstract: 	https://arxiv.org/abs/2309.14950

pdf_url:	https://arxiv.org/pdf/2309.14950

# FEC: Three Finetuning-free Methods to Enhance Consistency for Real Image  Editing 

authors: ['Songyan Chen', 'Jiancheng Huang']

abstract: 	https://arxiv.org/abs/2309.14934

pdf_url:	https://arxiv.org/pdf/2309.14934

# Addressing Data Misalignment in Image-LiDAR Fusion on Point Cloud  Segmentation 

authors: ['Wei Jong Yang', 'Guan Cheng Lee', 'arXiv:2309.11755']

abstract: 	https://arxiv.org/abs/2309.14932

pdf_url:	https://arxiv.org/pdf/2309.14932

# Noise-Tolerant Unsupervised Adapter for Vision-Language Models 

authors: ['Eman Ali', 'Dayan Guan', 'Shijian Lu', 'Abdulmotaleb Elsaddik']

abstract: 	https://arxiv.org/abs/2309.14928

pdf_url:	https://arxiv.org/pdf/2309.14928

